package cn.regent.wechat.authorization.service;

import cn.regent.wechat.authorization.model.WxAuthorizerInfo;

/**
 * Created by kk on 2017/11/17.
 */
public interface IWxAuthorizerInfoService {

    /**
     * 根据授权方Appid查询授权信息
     * @param authorizerAppId
     * @return
     */
    WxAuthorizerInfo getWxAuthorizerInfoByAuthAppId(String authorizerAppId);

    /**
     * 保存授权信息
     * @param wxAuthorizerInfo
     */
    void saveWxAuthorizerInfo(WxAuthorizerInfo wxAuthorizerInfo);

    /**
     * 更新授权信息
     * @param wxAuthorizerInfo
     */
    void updateWxAuthorizerInfo(WxAuthorizerInfo wxAuthorizerInfo);

}
