package cn.regent.wechat.authorization.service.impl;

import cn.regent.wechat.authorization.dao.WxAuthorizerInfoMapper;
import cn.regent.wechat.authorization.model.WxAuthorizerInfo;
import cn.regent.wechat.authorization.service.IWxAuthorizerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by kk on 2017/11/17.
 */
@Service
public class WxAuthorizerInfoServiceImpl implements IWxAuthorizerInfoService {

    @Autowired
    private WxAuthorizerInfoMapper wxAuthorizerInfoMapper;

    /**
     * 根据授权方Appid查询授权信息
     *
     * @param authorizerAppId
     * @return
     */
    @Override
    public WxAuthorizerInfo getWxAuthorizerInfoByAuthAppId(String authorizerAppId) {
        return wxAuthorizerInfoMapper.getWxAuthorizerInfoByAuthAppId(authorizerAppId);
    }

    /**
     * 保存授权信息
     *
     * @param wxAuthorizerInfo
     */
    @Override
    public void saveWxAuthorizerInfo(WxAuthorizerInfo wxAuthorizerInfo) {
        wxAuthorizerInfoMapper.saveWxAuthorizerInfo(wxAuthorizerInfo);
    }

    /**
     * 更新授权信息
     *
     * @param wxAuthorizerInfo
     */
    @Override
    public void updateWxAuthorizerInfo(WxAuthorizerInfo wxAuthorizerInfo) {
        wxAuthorizerInfoMapper.updateWxAuthorizerInfo(wxAuthorizerInfo);
    }
}
