package cn.regent.wechat.authorization.dao;

import cn.regent.wechat.authorization.model.WxConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by kk on 2017/11/17.
 */
@Mapper
@Repository
public interface WxConfigMapper {

    /**
     * 根据主键查询配置信息
     * @param id
     * @return
     */
    WxConfig getWxConfigById(@Param("id") String id);

    /**
     * 保存配置信息
     * @param wxConfig
     */
    void saveWxconfig(@Param("wxConfig") WxConfig wxConfig);

    /**
     * 更新配置信息
     * @param wxConfig
     */
    void updateWxconfig(@Param("wxConfig") WxConfig wxConfig);

}
