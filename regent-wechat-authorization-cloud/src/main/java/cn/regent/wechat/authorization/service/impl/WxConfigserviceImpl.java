package cn.regent.wechat.authorization.service.impl;

import cn.regent.wechat.authorization.dao.WxConfigMapper;
import cn.regent.wechat.authorization.model.WxConfig;
import cn.regent.wechat.authorization.service.IWxConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by kk on 2017/11/17.
 */
@Service
public class WxConfigserviceImpl implements IWxConfigService {

    @Autowired
    private WxConfigMapper wxConfigMapper;

    /**
     * 根据主键查询配置信息
     *
     * @param id
     * @return
     */
    @Override
    public WxConfig getWxConfigById(String id) {
        return wxConfigMapper.getWxConfigById(id);
    }

    /**
     * 保存配置信息
     *
     * @param wxConfig
     */
    @Override
    public void saveWxconfig(WxConfig wxConfig) {
        wxConfigMapper.saveWxconfig(wxConfig);
    }

    /**
     * 更新配置信息
     *
     * @param wxConfig
     */
    @Override
    public void updateWxconfig(WxConfig wxConfig) {
        wxConfigMapper.updateWxconfig(wxConfig);
    }
}
