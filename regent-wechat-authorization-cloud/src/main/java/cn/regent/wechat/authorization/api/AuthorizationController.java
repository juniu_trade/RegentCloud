package cn.regent.wechat.authorization.api;

import cn.regent.common.enumeration.ProjectInfoEnum;
import cn.regent.wechat.authorization.enumeration.ComponentEnum;
import cn.regent.wechat.authorization.model.WxAuthorizerInfo;
import cn.regent.wechat.authorization.model.WxComponent;
import cn.regent.wechat.authorization.model.WxConfig;
import cn.regent.wechat.authorization.service.IWxAuthorizerInfoService;
import cn.regent.wechat.authorization.service.IWxComponentService;
import cn.regent.wechat.authorization.service.IWxConfigService;
import cn.regent.wechat.authorization.service.impl.WxComponentServiceImpl;
import com.qq.weixin.mp.aes.AesException;
import com.qq.weixin.mp.aes.WXBizMsgCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import weixin.popular.api.ComponentAPI;
import weixin.popular.bean.component.*;
import weixin.popular.util.StreamUtils;
import weixin.popular.util.XMLConverUtil;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

/**
 * Created by kk on 2017/11/14.
 */
@RestController
@RequestMapping("/wx/auth")
public class AuthorizationController {
    private final static Logger log = LoggerFactory.getLogger(AuthorizationController.class);
    @Autowired
    private IWxComponentService wxComponentServiceImpl;
    @Autowired
    private IWxConfigService wxConfigserviceImpl;
    @Autowired
    private IWxAuthorizerInfoService wxAuthorizerInfoServiceImpl;



    @Value("${server.context-path}")
    private String appShortName;

    /**
     * 微信开放平台授权事件接收方法
     * @return
     */
    @RequestMapping(value = {"/api","/receive/url"})
    @ResponseBody
    public void receiveUrl(HttpServletRequest request, HttpServletResponse response) throws Exception{
        ServletInputStream inputStream = request.getInputStream();
        ServletOutputStream outputStream = response.getOutputStream();
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");

        //加密模式
        String encrypt_type = request.getParameter("encrypt_type");
        String msg_signature = request.getParameter("msg_signature");

        WXBizMsgCrypt wxBizMsgCrypt = null;
        //获取XML数据（含加密参数）
        String postData = StreamUtils.copyToString(inputStream, Charset.forName("utf-8"));
        System.out.println("timestamp="+timestamp);
        System.out.println("nonce="+nonce);
        System.out.println("encrypt_type="+encrypt_type);
        System.out.println("msg_signature="+msg_signature);
        System.out.println("postData="+postData);
        //加密方式
        boolean isAes = "aes".equals(encrypt_type);
        if (isAes) {
            try {
                wxBizMsgCrypt = new WXBizMsgCrypt(ComponentEnum.ENCODING_TOKEN.getValue(), ComponentEnum.ENCODING_AES_KEY.getValue(), ComponentEnum.COMPONENT_APP_ID.getValue());
                ////解密XML 数据
                postData = wxBizMsgCrypt.decryptMsg(msg_signature, timestamp, nonce, postData);

            } catch (AesException e) {
                log.info(e.getMessage(),e);
            }
        }
        //获取数据的map 格式
        Map<String, String> mapData = XMLConverUtil.convertToMap(postData);
        log.debug("==============================wxBizMsgCrypt is finished,mapData is:");
        log.debug("获取数据的map 格式",mapData);
        String infoType = mapData.get("InfoType");
        String infoAppId = mapData.get("AppId");
        if (infoType != null) {
            //微信服务器发送给服务自身的事件推送

            log.debug("==============================微信服务器发送给服务自身的事件推送 infoType=" + infoType + ",infoAppid=" + infoAppId);
            if (infoType.equals("authorized")) {

            } else if (infoType.equals("unauthorized")) {

            } else if (infoType.equals("updateauthorized")) {

            } else if (infoType.equals("component_verify_ticket")) {

                ComponentReceiveXML componentReceiveXML = XMLConverUtil.convertToObject(ComponentReceiveXML.class, postData);

                //log.debug("=================componentReceiveXML::" + componentReceiveXML);
                WxComponent wxComponent = wxComponentServiceImpl.findWxComponentByAppId(infoAppId);
                if (wxComponent != null) {
                    String componentVerifyTicket = componentReceiveXML.getComponentVerifyTicket();
                    //log.debug("=================componentVerifyTicket::" + componentVerifyTicket);
                    wxComponent.setComponentVerifyTicket(componentVerifyTicket);

                    ComponentAccessToken componentAccessToken = ComponentAPI.api_component_token(ComponentEnum.COMPONENT_APP_ID.getValue(), ComponentEnum.COMPONENT_APP_SECRET.getValue(), componentVerifyTicket);
                    //log.debug("=================componentAccessToken::" + componentAccessToken.getComponent_access_token());
                    wxComponent.setComponentAccessToken(componentAccessToken.getComponent_access_token());

                    PreAuthCode preauthcode = ComponentAPI.api_create_preauthcode(componentAccessToken.getComponent_access_token(), ComponentEnum.COMPONENT_APP_ID.getValue());
                    //log.debug("=================getPre_auth_code::" + preauthcode.getPre_auth_code());
                    wxComponent.setPreAuthCode(preauthcode.getPre_auth_code());



                    wxComponent.setOpAt(new Date());
                    wxComponent.setOpBy("WX_COMPONENT");

                    log.debug("=================Init update wxComponent,set componentAccessToken::" + wxComponent.getComponentAccessToken());


                    wxComponentServiceImpl.updateWxComponent(wxComponent);
                }

                outputStreamWrite(outputStream, "success");
            }
        }
    }

    /**
     * 数据流输出
     *
     * @param outputStream
     * @param text
     * @return
     */
    private boolean outputStreamWrite(OutputStream outputStream, String text) {
        try {
            outputStream.write(text.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @RequestMapping(value = {"/getComponentLoginPage"})
    @ResponseBody
    public void getComponentLoginPage(HttpServletRequest request,HttpServletResponse response) {
        try {
            WxComponent wxComponent = wxComponentServiceImpl.findWxComponentByAppId(ComponentEnum.COMPONENT_APP_ID.getValue());

            String back_url = "https://" + ProjectInfoEnum.APP_DOMAIN.getValue() + "/" + appShortName + "/wx/auth/" + ComponentEnum.COMPONENT_APP_ID.getValue() + "/back";

            String redirect_uri = back_url;
            log.debug("=================redirect_uri::" + redirect_uri);
            wxComponent.setCallbackUrl(redirect_uri);

            String componentLoginPage = ComponentAPI.componentloginpage(ComponentEnum.COMPONENT_APP_ID.getValue(), wxComponent.getPreAuthCode(), redirect_uri);
            log.debug("=================componentLoginPage::" + componentLoginPage);
            //wxComponent.setComponentLoginPage(componentLoginPage);

            response.sendRedirect(componentLoginPage);
        } catch (Exception e) {
            log.debug(e.getMessage(),e);
        }
    }

    @RequestMapping(value={"/?/back","/?/callback"})
    @ResponseBody
    public Object backToWx(@PathVariable("appId") String unitId,@RequestParam("auth_code") String authorizationCode){
        log.debug("================wx component back::" + authorizationCode + ":::::" + authorizationCode);

        String componentAppId = ComponentEnum.COMPONENT_APP_ID.getValue();
        WxComponent wxComponent = wxComponentServiceImpl.findWxComponentByAppId(componentAppId);

        String componentAccessToken = wxComponent.getComponentAccessToken();
        String wxComponentGotoUrl = wxComponent.getComponentHome();

        ApiQueryAuthResult apiQueryAuthResult = ComponentAPI.api_query_auth(componentAccessToken, componentAppId, authorizationCode);

        String authorizerAppId = apiQueryAuthResult.getAuthorization_info().getAuthorizer_appid();
        String authorizerRefreshToken = apiQueryAuthResult.getAuthorization_info().getAuthorizer_refresh_token();

        ApiGetAuthorizerInfoResult apiGetAuthorizerInfoResult = ComponentAPI.api_get_authorizer_info(componentAccessToken, componentAppId, authorizerAppId);

        log.debug("===================apiGetAuthorizerInfoResult:getAuthorizer_appid:" + apiGetAuthorizerInfoResult.getAuthorization_info().getAuthorizer_appid());
        log.debug("apiGetAuthorizerInfoResult",apiGetAuthorizerInfoResult);


        AuthorizerAccessToken authorizerAccessToken = ComponentAPI.api_authorizer_token(componentAccessToken, componentAppId, authorizerAppId, authorizerRefreshToken);

        log.debug("===================authorizerAccessToken:getAuthorizer_refresh_token:" + authorizerAccessToken.getAuthorizer_refresh_token());



        if (apiGetAuthorizerInfoResult != null && authorizerAccessToken != null) {

            ApiGetAuthorizerInfoResult.Authorizer_info authorizerInfo=apiGetAuthorizerInfoResult.getAuthorizer_info();
            String wxid=apiQueryAuthResult.getAuthorization_info().getAuthorizer_appid();

            WxConfig wxConfig=wxConfigserviceImpl.getWxConfigById(wxid);
            WxAuthorizerInfo wxAuthorizerInfo = wxAuthorizerInfoServiceImpl.getWxAuthorizerInfoByAuthAppId(wxid);


            if (wxConfig == null) {

                wxConfig=new WxConfig();
                wxConfig.setId(wxid);
                wxConfig.setAppId(componentAppId);
                wxConfig.setGhid(authorizerInfo.getUser_name());
                wxConfig.setComponentAppId(componentAppId);
                wxConfig.setAppName(authorizerInfo.getPrincipal_name());
                wxConfig.setAppSecret(ComponentEnum.COMPONENT_APP_SECRET.getValue());
                wxConfig.setEncodingAesKey(ComponentEnum.ENCODING_AES_KEY.getValue());
                wxConfig.setToken(ComponentEnum.ENCODING_TOKEN.getValue());
                wxConfig.setUnitId(unitId);



                wxConfig.setAccessToken(authorizerAccessToken.getAuthorizer_access_token());
                wxConfig.setAccessRefreshToken(authorizerAccessToken.getAuthorizer_refresh_token());
                wxConfig.setAccessTokenExpires(authorizerAccessToken.getExpires_in());

                wxConfig.setCreateAt(new Date());
                wxConfig.setOpBy(apiGetAuthorizerInfoResult.getAuthorization_info().getAuthorizer_appid());

                wxConfigserviceImpl.saveWxconfig(wxConfig);

                wxAuthorizerInfo = new WxAuthorizerInfo();
                wxAuthorizerInfo.setAuthorizerAccessToken(authorizerAccessToken.getAuthorizer_access_token());
                wxAuthorizerInfo.setExpiresIn(authorizerAccessToken.getExpires_in());
                wxAuthorizerInfo.setAuthorizerRefreshToken(authorizerAccessToken.getAuthorizer_refresh_token());

                ApiGetAuthorizerInfoResult.Authorization_info authorizationInfo=apiGetAuthorizerInfoResult.getAuthorization_info();
                wxAuthorizerInfo.setAuthorizerAppId(authorizationInfo.getAuthorizer_appid());
                String listString = "";
                for ( FuncInfo functionInfo : authorizationInfo.getFunc_info())
                {
                    listString += functionInfo.getFuncscope_category().getId() + ",";
                }
                wxAuthorizerInfo.setFuncInfo(listString);


                wxAuthorizerInfo.setQrcodeUrl(authorizerInfo.getQrcode_url());
                wxAuthorizerInfo.setVerifyTypeInfo(authorizerInfo.getVerify_type_info().toString());
                wxAuthorizerInfo.setAlias(authorizerInfo.getAlias());
                wxAuthorizerInfo.setBusinessInfo(authorizerInfo.getBusiness_info().getOpen_pay().toString());
                wxAuthorizerInfo.setHeadImg(authorizerInfo.getHead_img());
                wxAuthorizerInfo.setNickName(authorizerInfo.getNick_name());
                wxAuthorizerInfo.setPrincipalName(authorizerInfo.getPrincipal_name());
                wxAuthorizerInfo.setServiceTypeInfo(authorizerInfo.getService_type_info().toString());
                wxAuthorizerInfo.setUserName(authorizerInfo.getUser_name());

                wxAuthorizerInfo.setCreateAt(new Date());

                wxAuthorizerInfo.setOpBy(apiGetAuthorizerInfoResult.getAuthorization_info().getAuthorizer_appid());

                wxAuthorizerInfoServiceImpl.saveWxAuthorizerInfo(wxAuthorizerInfo);

            } else {


//                wxConfig.setComponentAppId(componentAppId);
                wxConfig.setAppName(authorizerInfo.getPrincipal_name());

                wxConfig.setAccessToken(authorizerAccessToken.getAuthorizer_access_token());
                wxConfig.setAccessRefreshToken(authorizerAccessToken.getAuthorizer_refresh_token());
                wxConfig.setAccessTokenExpires(authorizerAccessToken.getExpires_in());
                wxConfig.setUnitId(unitId);

                wxConfig.setOpAt(new Date());
                wxAuthorizerInfo.setOpBy(apiGetAuthorizerInfoResult.getAuthorization_info().getAuthorizer_appid());

                wxConfigserviceImpl.updateWxconfig(wxConfig);


                wxAuthorizerInfo.setAuthorizerAccessToken(authorizerAccessToken.getAuthorizer_access_token());
                wxAuthorizerInfo.setExpiresIn(authorizerAccessToken.getExpires_in());
                wxAuthorizerInfo.setAuthorizerRefreshToken(authorizerAccessToken.getAuthorizer_refresh_token());

                ApiGetAuthorizerInfoResult.Authorization_info authorizationInfo=apiGetAuthorizerInfoResult.getAuthorization_info();
                wxAuthorizerInfo.setAuthorizerAppId(authorizationInfo.getAuthorizer_appid());
                String listString = "";
                for ( FuncInfo functionInfo : authorizationInfo.getFunc_info())
                {
                    listString += functionInfo.getFuncscope_category().getId() + ",";
                }
                wxAuthorizerInfo.setFuncInfo(listString);


                wxAuthorizerInfo.setQrcodeUrl(authorizerInfo.getQrcode_url());
                wxAuthorizerInfo.setVerifyTypeInfo(authorizerInfo.getVerify_type_info().toString());
                wxAuthorizerInfo.setAlias(authorizerInfo.getAlias());
                wxAuthorizerInfo.setBusinessInfo(authorizerInfo.getBusiness_info().getOpen_pay().toString());
                wxAuthorizerInfo.setHeadImg(authorizerInfo.getHead_img());
                wxAuthorizerInfo.setNickName(authorizerInfo.getNick_name());
                wxAuthorizerInfo.setPrincipalName(authorizerInfo.getPrincipal_name());
                wxAuthorizerInfo.setServiceTypeInfo(authorizerInfo.getService_type_info().toString());
                wxAuthorizerInfo.setUserName(authorizerInfo.getUser_name());


                wxAuthorizerInfo.setOpAt(new Date());
                wxAuthorizerInfo.setOpBy(apiGetAuthorizerInfoResult.getAuthorization_info().getAuthorizer_appid());

                wxAuthorizerInfoServiceImpl.updateWxAuthorizerInfo(wxAuthorizerInfo);

            }
//            session.setAttribute("componentAppId", componentAppId);
//            session.setAttribute("authorizerAppId", authorizerAppId);
//            session.setAttribute("authorizerAccessToken", wxAuthorizerInfo.getAuthorizerAccessToken());
            return "redirect:" + wxComponentGotoUrl;
        } else
            return "redirect:" + wxComponentGotoUrl+"/error";
    }

    @RequestMapping(value={"/list"})
    @ResponseBody
    public Object finaAll(){
        return ((WxComponentServiceImpl)wxComponentServiceImpl).findAll();
    }


}
