package cn.regent.wechat.authorization.dao;

import cn.regent.wechat.authorization.model.WxAuthorizerInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * Created by kk on 2017/11/17.
 */
@Mapper
@Component
public interface WxAuthorizerInfoMapper {

    /**
     * 根据授权方Appid查询授权信息
     * @param authorizerAppId
     * @return
     */
    WxAuthorizerInfo getWxAuthorizerInfoByAuthAppId(@Param("authorizerAppId") String authorizerAppId);

    /**
     * 保存授权信息
     * @param wxAuthorizerInfo
     */
    void saveWxAuthorizerInfo(@Param("wxAuthorizerInfo") WxAuthorizerInfo wxAuthorizerInfo);

    /**
     * 更新授权信息
     * @param wxAuthorizerInfo
     */
    void updateWxAuthorizerInfo(@Param("wxAuthorizerInfo") WxAuthorizerInfo wxAuthorizerInfo);

}
