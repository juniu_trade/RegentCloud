package cn.regent.wechat.authorization.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kk on 2017/11/16.
 */
public class WxComponent implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String appName;
    private String componentAppId;
    private String componentAppSecret;
    private String encodingAesKey;
    private String encodingToken;
    private String componentVerifyicket;
    private String componentAccessToken;
    private String preAuthCode;
    private String componentLoginPage;
    private String componentHome;
    private String callbackUrl;
    private Date createAt;
    private String opBy;
    private Date opAt;
    private Integer delFlag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getComponentAppId() {
        return componentAppId;
    }

    public void setComponentAppId(String componentAppId) {
        this.componentAppId = componentAppId;
    }

    public String getComponentAppSecret() {
        return componentAppSecret;
    }

    public void setComponentAppSecret(String componentAppSecret) {
        this.componentAppSecret = componentAppSecret;
    }

    public String getEncodingAesKey() {
        return encodingAesKey;
    }

    public void setEncodingAesKey(String encodingAesKey) {
        this.encodingAesKey = encodingAesKey;
    }

    public String getEncodingToken() {
        return encodingToken;
    }

    public void setEncodingToken(String encodingToken) {
        this.encodingToken = encodingToken;
    }

    public String getComponentVerifyTicket() {
        return componentVerifyicket;
    }

    public void setComponentVerifyTicket(String componentVerifyTicket) {
        this.componentVerifyicket = componentVerifyTicket;
    }

    public String getComponentAccessToken() {
        return componentAccessToken;
    }

    public void setComponentAccessToken(String componentAccessToken) {
        this.componentAccessToken = componentAccessToken;
    }

    public String getPreAuthCode() {
        return preAuthCode;
    }

    public void setPreAuthCode(String preAuthCode) {
        this.preAuthCode = preAuthCode;
    }

    public String getComponentLoginPage() {
        return componentLoginPage;
    }

    public void setComponentLoginPage(String componentLoginPage) {
        this.componentLoginPage = componentLoginPage;
    }

    public String getComponentHome() {
        return componentHome;
    }

    public void setComponentHome(String componentHome) {
        this.componentHome = componentHome;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getOpBy() {
        return opBy;
    }

    public void setOpBy(String opBy) {
        this.opBy = opBy;
    }

    public Date getOpAt() {
        return opAt;
    }

    public void setOpAt(Date opAt) {
        this.opAt = opAt;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}
