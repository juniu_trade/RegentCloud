package cn.regent.wechat.authorization.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by kk on 2017/11/17.
 */
public class WxConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String componentAppId;
    private String appName;
    private String ghid;
    private String appId;
    private String appSecret;
    private String encodingAesKey;
    private String token;
    private String accessToken;
    private Integer accessTokenExpires;
    private String accessTokenLastat;
    private String accessRefreshToken;
    private boolean payEnabled;
    private String payInfo;
    private String unitId;
    private Date createAt;
    private String opBy;
    private Date opAt;
    private Integer delFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComponentAppId() {
        return componentAppId;
    }

    public void setComponentAppId(String componentAppId) {
        this.componentAppId = componentAppId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getGhid() {
        return ghid;
    }

    public void setGhid(String ghid) {
        this.ghid = ghid;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getEncodingAesKey() {
        return encodingAesKey;
    }

    public void setEncodingAesKey(String encodingAesKey) {
        this.encodingAesKey = encodingAesKey;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getAccessTokenExpires() {
        return accessTokenExpires;
    }

    public void setAccessTokenExpires(Integer accessTokenExpires) {
        this.accessTokenExpires = accessTokenExpires;
    }

    public String getAccessTokenLastat() {
        return accessTokenLastat;
    }

    public void setAccessTokenLastat(String accessTokenLastat) {
        this.accessTokenLastat = accessTokenLastat;
    }

    public String getAccessRefreshToken() {
        return accessRefreshToken;
    }

    public void setAccessRefreshToken(String accessRefreshToken) {
        this.accessRefreshToken = accessRefreshToken;
    }

    public boolean isPayEnabled() {
        return payEnabled;
    }

    public void setPayEnabled(boolean payEnabled) {
        this.payEnabled = payEnabled;
    }

    public String getPayInfo() {
        return payInfo;
    }

    public void setPayInfo(String payInfo) {
        this.payInfo = payInfo;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getOpBy() {
        return opBy;
    }

    public void setOpBy(String opBy) {
        this.opBy = opBy;
    }

    public Date getOpAt() {
        return opAt;
    }

    public void setOpAt(Date opAt) {
        this.opAt = opAt;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}
