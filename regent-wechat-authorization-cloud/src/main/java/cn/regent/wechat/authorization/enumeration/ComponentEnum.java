package cn.regent.wechat.authorization.enumeration;

/**
 * 定义枚举类一定要给出相应的构造方法
 * Created by kk on 2017/11/16.
 */
public enum ComponentEnum {
    COMPONENT_APP_ID("wx371229253f8141e1","微信开放平台第三方绑定AppID"),
    COMPONENT_APP_SECRET("35feeadfa52e07196db56478b64a303e","微信开放平台第三方绑定AppSecret"),
    ENCODING_TOKEN("regentcloud147852369","第三方平台申请时填写的接收消息的校验token"),
    ENCODING_AES_KEY("regentcloud98765432101234567891478523699876","第三方平台申请时填写的接收消息的加密");

    private String value;
    private String text;

    ComponentEnum(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
