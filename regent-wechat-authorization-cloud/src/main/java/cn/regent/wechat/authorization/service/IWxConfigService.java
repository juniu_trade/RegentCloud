package cn.regent.wechat.authorization.service;

import cn.regent.wechat.authorization.model.WxConfig;

/**
 * Created by kk on 2017/11/17.
 */
public interface IWxConfigService {

    /**
     * 根据主键查询配置信息
     * @param id
     * @return
     */
    WxConfig getWxConfigById(String id);

    /**
     * 保存配置信息
     * @param wxConfig
     */
    void saveWxconfig(WxConfig wxConfig);

    /**
     * 更新配置信息
     * @param wxConfig
     */
    void updateWxconfig(WxConfig wxConfig);

}
