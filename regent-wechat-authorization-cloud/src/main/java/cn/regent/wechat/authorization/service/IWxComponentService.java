package cn.regent.wechat.authorization.service;

import cn.regent.wechat.authorization.model.WxComponent;

/**
 * Created by kk on 2017/11/16.
 */
public interface IWxComponentService {
    /**
     * 查询第三方平台信息
     * @param componentAppId  第三方平台的AppID
     * @return
     */
    WxComponent findWxComponentByAppId(String componentAppId);

    /**
     * 更新第三方平台信息
     * @param wxComponent
     * @return
     */
    int updateWxComponent(WxComponent wxComponent);
}
