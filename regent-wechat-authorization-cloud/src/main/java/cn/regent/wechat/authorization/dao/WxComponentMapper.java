package cn.regent.wechat.authorization.dao;

import cn.regent.wechat.authorization.model.WxComponent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by kk on 2017/11/16.
 */
@Mapper
@Component(value="wxComponentMapper")
public interface WxComponentMapper {

    List<WxComponent> findAll();

    /**
     * 查询第三方平台信息
     * @return
     */
    WxComponent findWxComponentByAppId(@Param("mapParam") Map mapParam);

    /**
     * 更新第三方平台信息
     * @param wxComponent
     * @return
     */
    int updateWxComponent(@Param("wxComponent") WxComponent wxComponent);

}
