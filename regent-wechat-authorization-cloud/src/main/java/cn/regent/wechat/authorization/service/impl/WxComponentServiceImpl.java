package cn.regent.wechat.authorization.service.impl;

import cn.regent.wechat.authorization.dao.WxComponentMapper;
import cn.regent.wechat.authorization.model.WxComponent;
import cn.regent.wechat.authorization.service.IWxComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by kk on 2017/11/16.
 */
@Service
public class WxComponentServiceImpl implements IWxComponentService {

    @Autowired
    private WxComponentMapper wxComponentMapper;

    public Object findAll(){
        return wxComponentMapper.findAll();
    }


    /**
     * 查询第三方平台信息
     *
     * @param componentAppId 第三方平台的AppID
     * @return
     */
    @Override
    public WxComponent findWxComponentByAppId(String componentAppId) {
        Map mapParam = new HashMap();
        mapParam.put("componentAppId",componentAppId);
        return wxComponentMapper.findWxComponentByAppId(mapParam);
    }

    /**
     * 更新第三方平台信息
     *
     * @param wxComponent
     * @return
     */
    @Override
    public int updateWxComponent(WxComponent wxComponent) {
        return wxComponentMapper.updateWxComponent(wxComponent);
    }
}
