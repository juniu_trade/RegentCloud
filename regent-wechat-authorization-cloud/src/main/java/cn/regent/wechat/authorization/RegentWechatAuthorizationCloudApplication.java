package cn.regent.wechat.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegentWechatAuthorizationCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegentWechatAuthorizationCloudApplication.class, args);
	}
}
