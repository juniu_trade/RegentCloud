package cn.regent.wechat.authorization;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * 修改启动类，继承 SpringBootServletInitializer 并重写 configure 方法
 * Created by kk on 2017/11/21.
 */
public class SpringBootStartApplication extends SpringBootServletInitializer {

    @Override
    public SpringApplicationBuilder configure(SpringApplicationBuilder builder){
        return builder.sources(RegentWechatAuthorizationCloudApplication.class);
    }

}
