/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : regentcloud

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-11-21 12:28:31
*/

CREATE DATABASE IF NOT EXISTS `regentcloud` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `regentcloud`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for wx_authorizer_info
-- ----------------------------
DROP TABLE IF EXISTS `wx_authorizer_info`;
CREATE TABLE `wx_authorizer_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `nick_name` varchar(120) DEFAULT NULL COMMENT '授权方昵称',
  `head_img` varchar(500) DEFAULT NULL COMMENT '授权方头像',
  `service_type_info` varchar(500) DEFAULT NULL COMMENT '默认为0',
  `verify_type_info` varchar(500) DEFAULT NULL COMMENT '授权方认证类型，-1代表未认证，0代表微信认证',
  `user_name` varchar(500) DEFAULT NULL COMMENT '原始ID',
  `principal_name` varchar(100) DEFAULT NULL COMMENT '公众号的主体名称',
  `business_info` varchar(500) DEFAULT NULL COMMENT '用以了解以下功能的开通状况（0代表未开通，1代表已开通）：\\n open_store:是否开通微信门店功能\\n open_scan:是否开通微信扫商品功能\\n open_pay:是否开通微信支付功能\\n open_card:是否开通微信卡券功能\\n open_shake:是否开通微信摇一摇功能',
  `qrcode_url` varchar(500) DEFAULT NULL COMMENT '二维码图片的URL，开发者最好自行也进行保存',
  `signature` varchar(500) DEFAULT NULL COMMENT '小程序签名',
  `mini_program_info` varchar(500) DEFAULT NULL COMMENT '可根据这个字段判断是否为小程序类型授权',
  `network` varchar(500) DEFAULT NULL COMMENT '小程序已设置的各个服务器域名',
  `alias` varchar(200) DEFAULT NULL COMMENT '授权方公众号所设置的微信号，可能为空',
  `func_info` varchar(500) DEFAULT NULL COMMENT '授权方开放权限列表',
  `authorizer_app_id` varchar(50) DEFAULT NULL COMMENT '授权方Appid',
  `authorizer_access_token` varchar(255) DEFAULT NULL COMMENT '授权方令牌',
  `expires_in` int(32) DEFAULT NULL COMMENT '有效期，为2小时',
  `authorizer_refresh_token` varchar(255) DEFAULT NULL COMMENT '刷新令牌',
  `create_at` datetime DEFAULT NULL,
  `op_by` varchar(32) DEFAULT NULL COMMENT '操作人',
  `op_at` datetime DEFAULT NULL COMMENT '操作时间',
  `del_flag` tinyint(1) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `IDX_AUTHORIZER_APP_ID` (`authorizer_app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公众号授权给第三方信息';

-- ----------------------------
-- Records of wx_authorizer_info
-- ----------------------------

-- ----------------------------
-- Table structure for wx_component
-- ----------------------------
DROP TABLE IF EXISTS `wx_component`;
CREATE TABLE `wx_component` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `app_name` varchar(120) DEFAULT NULL COMMENT '第三方平台名称',
  `component_app_id` varchar(50) DEFAULT NULL COMMENT '第三方平台Appid',
  `component_app_secret` varchar(50) DEFAULT NULL COMMENT '第三方平台Appsecret',
  `encoding_aes_key` varchar(100) DEFAULT NULL COMMENT '第三方平台申请时填写的接收消息的加密',
  `encoding_token` varchar(100) DEFAULT NULL COMMENT '第三方平台申请时填写的接收消息的校验token',
  `component_verify_ticket` varchar(1000) DEFAULT NULL COMMENT '第三方平台调用凭证Ticket',
  `component_access_token` varchar(1000) DEFAULT NULL COMMENT '第三方平台调用凭证',
  `pre_auth_code` varchar(1000) DEFAULT NULL COMMENT '第三方平台调用凭证code',
  `component_login_page` varchar(1000) DEFAULT NULL COMMENT '生成的第三方平台授权页面URL',
  `component_home` varchar(50) DEFAULT NULL COMMENT '授权后跳转到的结果页',
  `callback_url` varchar(200) DEFAULT NULL COMMENT '回调地址',
  `create_at` datetime DEFAULT NULL COMMENT '操作时间',
  `op_by` varchar(32) DEFAULT NULL COMMENT '操作人',
  `op_at` datetime DEFAULT NULL COMMENT '操作时间',
  `del_flag` tinyint(1) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `IDX_COMPONENT_APP_ID` (`component_app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='第三方信息';

-- ----------------------------
-- Records of wx_component
-- ----------------------------
INSERT INTO `wx_component` VALUES ('1', '丽晶云平台', 'wx371229253f8141e1', '35feeadfa52e07196db56478b64a303e', 'regentcloud98765432101234567891478523699876', 'regentcloud147852369', 'ticket@@@PWhxXoIQzD528TzIcgaJyuWw6gg5z58DaRPzHRxho9YPYyWwy02K20SC8m7Zn4HtsGPoBGU7oNK4GcWKf0ZiSg', 'yTgRgGcCBsMVBas1VobqSvTlt2UQd-LuJd_yjQ-XHj-SIw8dw0tQoBlvOkHjWjnSMGCGbSnbvDAm_IJnX50EP-MbI3Pct2T0D9YAtxTfCXD9BnsNAT9baycAHY1RnkyWYOIiAFAEUO', 'preauthcode@@@DLBoZq87oWxkZhT_3GF3gbCTK83BAhUJzE806XQ38llrFLZfFhzibxylTm33YOZM', null, 'https://market-beta.juniusoft.com/new-mf', 'https://market-beta.juniusoft.com/new-mf/wx/component/wx371229253f8141e1/back', '2017-11-16 16:07:28', 'WX_COMPONENT', '2017-11-20 10:38:09', null);

-- ----------------------------
-- Table structure for wx_config
-- ----------------------------
DROP TABLE IF EXISTS `wx_config`;
CREATE TABLE `wx_config` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `component_app_id` varchar(32) NOT NULL DEFAULT '' COMMENT 'ID',
  `app_name` varchar(120) DEFAULT NULL COMMENT '公众号名称',
  `ghid` varchar(50) DEFAULT NULL COMMENT '原始ID',
  `app_id` varchar(50) DEFAULT NULL COMMENT 'Appid',
  `app_secret` varchar(50) DEFAULT NULL COMMENT 'Appsecret',
  `encoding_aes_key` varchar(100) DEFAULT NULL COMMENT 'EncodingAESKey',
  `token` varchar(100) DEFAULT NULL COMMENT 'Token',
  `access_token` varchar(600) DEFAULT NULL COMMENT 'access_token',
  `access_token_expires` int(11) DEFAULT NULL COMMENT 'access token expires',
  `access_token_lastat` int(11) DEFAULT NULL COMMENT 'access token lastat',
  `access_refresh_token` varchar(600) DEFAULT NULL COMMENT 'access refresh token ',
  `pay_enabled` tinyint(1) DEFAULT NULL COMMENT '禁用支付',
  `pay_info` text COMMENT '支付信息',
  `unit_id` varchar(32) DEFAULT NULL COMMENT '关联组织的ID',
  `create_at` datetime DEFAULT NULL COMMENT '操作时间',
  `op_by` varchar(32) DEFAULT NULL COMMENT '操作人',
  `op_at` datetime DEFAULT NULL COMMENT '操作时间',
  `del_flag` tinyint(1) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='第三方和公众号的配置信息';

-- ----------------------------
-- Records of wx_config
-- ----------------------------
