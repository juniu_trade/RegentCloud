package cn.regent.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegentCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegentCommonApplication.class, args);
	}
}
