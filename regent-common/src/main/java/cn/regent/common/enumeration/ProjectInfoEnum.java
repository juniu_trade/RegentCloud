package cn.regent.common.enumeration;

/**
 * Created by kk on 2017/11/17.
 */
public enum ProjectInfoEnum {
    APP_ROOT("",""),
    APP_BASE("",""),
    APP_SHORT_NAME("regentcloud","项目短名称"),
    APP_DOMAIN("qqzad758820.ngrok.wendal.cn","项目运行域名");

    private String value;
    private String text;

    /**
     * 枚举中的构造方法默认是private
     * @param value
     * @param text
     */
    private ProjectInfoEnum(String value,String text){
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
