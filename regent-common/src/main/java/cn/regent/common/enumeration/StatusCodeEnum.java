package cn.regent.common.enumeration;

/**
 * Created by kk on 2017/11/17.
 */
public enum StatusCodeEnum {
    SUCCESS(200,"成功"),
    ERROR(500,"后台出错");

    private int code;
    private String text;

    StatusCodeEnum(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
